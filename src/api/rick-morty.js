const _doRequest = async (uri) => {
  try {
    const response = await fetch(uri);
    const data = await response.json();
    return data;
  } catch (error) {
    throw new Error(error);
  }
};

const rickandmortyapi = {
  async getInitialData() {
    const uri = "https://rickandmortyapi.com/api/character";
    return await _doRequest(uri);
  },
  async getCharacter(characterURI) {
    return await _doRequest(characterURI);
  },
  async getPage(pageURI) {
    return await _doRequest(pageURI);
  },
};

export default rickandmortyapi;
