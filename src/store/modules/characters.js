import rickandmortyapi from "@/api/rick-morty";

// initial state
const state = () => ({
  characters: null,
  metadata: null,
  page: 0,
});

// getters
const getters = {
  getMetadata(state) {
    return state.metadata;
  },
  getCurrentPage(state) {
    return state.page;
  },
};

// mutations
const mutations = {
  setCharacters(state, characters) {
    state.characters = characters;
  },
  setMetadata(state, metadata) {
    state.metadata = metadata;
  },
  setPage(state, n) {
    state.page = n;
  },
  getCharacters(state) {
    return state.characters;
  },
};

// actions
const actions = {
  async getInitialCharacters({ commit }) {
    const data = await rickandmortyapi.getInitialData();
    const { info, results } = data;
    commit("setCharacters", results);
    commit("setMetadata", info);
  },
  setPage({ commit }, n) {
    commit("setPage", n);
  },
  async loadPage({ commit }, uri) {
    const data = await rickandmortyapi.getPage(uri);
    const { info, results } = data;
    commit("setCharacters", results);
    commit("setMetadata", info);
    const regexpPage = new RegExp(/[0-9]+$/);
    const page = uri.match(regexpPage).shift();
    commit("setPage", page);
  },
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};
