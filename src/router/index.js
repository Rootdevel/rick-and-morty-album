import { createRouter, createWebHashHistory } from "vue-router";
//const MarvelView = () => import("@/views/MarvelView");
import MarvelView from "@/views/MarvelView.vue";
import AboutView from "@/views/AboutView.vue";

const routes = [
  {
    path: "/",
    name: "home",
    component: MarvelView,
    alias: ["/home", "/index"],
  },
  {
    path: "/about",
    name: "about",
    component: AboutView,
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
  strict: true,
});

export default router;
